-- captures
local capi = {
    mouse = mouse,
    screen = screen,
}
local awful = require("awful")
local naughty = require("naughty")

local gtd_next_items = {}

function gtd_next_items:new(args)
	return setmetatable({}, {__index = self}):init(args)
end

function gtd_next_items:init(args)
	self.position = args.position or naughty.config.defaults.position

	return self
end

function gtd_next_items:show()
	-- Make initial popup to indicate it was registered
	self.notification = naughty.notify({
		title = "Next Items",
		text = "Loading GTD...",
		timeout = 0,
		hover_timeout = 0.5,
		screen = capi.mouse.screen,
		position = self.position,
	})

	-- Update the popup to show GTD data
	awful.spawn.easy_async(
		"gtd next",
		function (out, err, reason, code)
			self:hide()
			self.notification = naughty.notify({
				title = "Next Items",
				text = out:gsub("%s+$", ""),
				timeout = 0,
				hover_timeout = 0.5,
				screen = capi.mouse.screen,
				position = self.position,
			})
		end
	)
end

function gtd_next_items:hide()
	if not self.notification then
		return
	end

	naughty.destroy(self.notification)
	self.notification = nil
	self.num_lines = 0
end

function gtd_next_items:attach(widget)
	widget:connect_signal('mouse::enter', function() self:show() end)
	widget:connect_signal('mouse::leave', function() self:hide() end)
end

return setmetatable(gtd_next_items, {
	__call = gtd_next_items.new,
})
