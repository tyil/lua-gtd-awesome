# GTD Next Items for Awesome

This is a small plugin, which will allow you to get your list of Next Items
displayed in Awesome. It is intended to be used with
[`App::GTD`](https://gitlab.com/tyil/perl6-App-GTD).

## Installation

Clone this repository in your Awesome configuration directory, and add a small
bit of configuration to your `rc.lua` to display it when hovering over the
clock/calendar.

    cd ~/.config/aweomse
    git clone https://gitlab.com/tyil/lua-gtd-awesome.git gtd_next_items
    $EDITOR rc.lua

    # Add this somewhere near the top
    local gtd_next_items = require("gtd_next_items")

    # Add this below your clock initialization code
    gtd_next_items({}):attach(mytextclock)

## License

This program is distributed under the terms of the GNU Affero General Public
License version 3.
